Studentnummer: 326134
Naam: Jasper Lingeman


Wat is git?
	# Versiebeheer systeem

Waar kan je een repository voor gebruiken?
	# Bijhouden van een project

Wat kan je met een commit?
	# Een savepoint binnen het project

Zoek de volgende commando's op:
 - Creëren van een repository
	# git init
 - Bestanden toevoegen aan stage area
	# git add pad/naar/bestand.txt
 - Bestanden committen
	# git  commit -m "Bericht van de commit."